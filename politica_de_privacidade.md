**Polítcia de Privacidade**

Geobit desenvolveu o aplicativo Árvores do Rio Doce como um app gratuito. Esse SERVIÇO é provido pela Geobit sem custos e deve ser usado como está.

Essa página é utilizada para informar aqueles que a visitam sobre as nossas políticas de coleta, uso e divulgação de informações pessoais no caso de decidirem utilizar nosso Serviço.

Se você escolher usar nosso Serviço, você concorda com a coleção e uso de informações relacionadas a essa política. As informações pessoais que coletamos são utilizadas para fornecer e melhorar nosso Serviço. Nós não iremos utilizar ou compartilhar suas informações com ninguém exceto como descrito nesssa Política de Privacidade.

Os termos usados nessa Política de Privacidade têm os mesmos objetivos que em nossos Termos e Condições, que está disponível no aplicativo Árvores do Rio Doce a não ser que isso seja definido de outra forma nessa Política de Privacidade.

**Coleta e Uso de Informações**

Para uma melhor experiência durante o uso do nosso Serviço, é possível que peçamos algumas informações pessoais, incluindo mas não limitada a fotos e arquivos. A informação que pedimos será retida por nós e usada como descrito nessa Política de Privacidade.

O aplicativo usa serviços de terceiros que podem coletar informaçoes utilizadas para identificar você.

Link para a Política de Privacide de serviços de terceiros usados pelo aplicativo

*   [Google Play Services](https://www.google.com/policies/privacy/)

**Dados de Log**

Queremos informar que quando você usa nosso Serviço, em caso de erro no aplicativo nós coletamos dados e informações (por meio de produtos de terceiros) em seu dispositivo chamadas Log. Esse Log pode incluir informações como o endereço IP ("Internet Protocol") do seu dispositivo, o nome do seu dispositivo, a versão do sistema operacional, a configuração do aplicativo quando utilizando nosso Serviço, a data e a hora do seu uso do Serviço, entre outras estatísticas.

**Cookies**

Cookies são arquivos com pequenas quantidades de dados que são utilizados como identificadores únicos anônimos. Eles são enviados para seu navegador pelos sites que você visita e são armazenados na memória interna do seu dispositivo.

**Provedores de Serviço**

É possível que utilizemos terceiros (companhias ou indivíduos) para as seguintes razões:

*   Para facilitar o nosso Serviço;
*   Para fornecer o Serviço em nosso nome;
*   Para efetuar serviços relacionados ao Serviço; ou
*   Para nos auxiliar na análise de como nosso serviço está sendo utilizado.

Queremos informar os usuários desse Serviço que esses terceiros têm acesso às suas Informações Pessoais. A razão para isso é para que seja possível efetuar as tarefas a eles designadas por nós. Entretanto, eles são obrigados a não divulgar ou utilizar essas informações para qualquqer outro propósito.

**Segurança**

Nós valorizamos sua confiança em nos fornecer Informações Pessoais, portanto nos esforçamos ao máximo encontrar formas comercialmente viáveis de protegê-la. Mas, lembre-se que nenhum método de transmissão de informação via Internet, ou método eletrônico de armazenamento, é 100% seguro e confiável, e nós não podemos garantir sua absoluta segurança.

**Links Para Outros Sites**

Esse serviço pode conter links para outros stes. Se você clica em um link de um terceiro, você será redirecionado para esse site. Note que esses sites externos não são operados por nós. Tendo isso em vista, nós aconselhamos fortemente que você veja a Política de Privacidade desses sites. Nós não temos controle sobre e não assumimos responsabilidade pelo conteúdo, políticas de privacidade ou práticas de nenhum site ou serviço de terceiros.

**Privacidade Infantil**

Nossos Serviços não aceitam pessoas com idade inferior a 13 anos. Nós não coletamos conscientemente Informações Pessoais de crianças menores de 13 anos\. Em caso de descobrirmos que uma criança com menos de 13 anos nos forneceu Informações Pessoais, iremos excluir imediatamente dos nossos servidores. Se você é um pai ou responsável e sabe que seu filho ou filha nos forneceu Informações Pessoais, por favor entre em contato conosco para que possamos tomar as decisões cabíveis.

**Mudanças Nessa Política de Privacidade**

É possível que atualizemos nossa Política de Privacidade de tempos em tempos. Por isso, revise essa página periodicamente para ver quaisquer mudanças. Nós iremos notificar você sobre qualquer mudança postando uma nova Política de Privacidade nessa página.

Essa Política de Privacidade tem efeito a partir de 09/11/2020.

**Entre em Contato Conoso**

Se você tem qualquer questão ou sugestão sobre nossa Política de Privacidade, não hesite em entrar em contato pelo e-mail geobitbr@gmail.com.

__________________________

**Privacy Policy**

Geobit built the Árvores do Rio Doce app as a Free app. This SERVICE is provided by Geobit at no cost and is intended for use as is.

This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.

If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.

The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Árvores do Rio Doce unless otherwise defined in this Privacy Policy.

**Information Collection and Use**

For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to photos and files. The information that we request will be retained by us and used as described in this privacy policy.

The app does use third party services that may collect information used to identify you.

Link to privacy policy of third party service providers used by the app

*   [Google Play Services](https://www.google.com/policies/privacy/)

**Log Data**

We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.

**Cookies**

Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.

This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.

**Service Providers**

We may employ third-party companies and individuals due to the following reasons:

*   To facilitate our Service;
*   To provide the Service on our behalf;
*   To perform Service-related services; or
*   To assist us in analyzing how our Service is used.

We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.

**Security**

We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.

**Links to Other Sites**

This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.

**Children’s Privacy**

These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13\. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.

**Changes to This Privacy Policy**

We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.

This policy is effective as of 2020-11-09

**Contact Us**

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at geobitbr@gmail.com.
